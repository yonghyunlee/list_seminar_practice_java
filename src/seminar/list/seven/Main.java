package seminar.list.seven;


public class Main {
    public static void main(String[] args) {
        DirectArrayGraph directArrayGraph = new DirectArrayGraph(5);
        directArrayGraph.addEdge(0, 1);
        directArrayGraph.addEdge(0, 2);
        directArrayGraph.addEdge(0, 3);
        directArrayGraph.addEdge(2, 4);

        directArrayGraph.display();

        directArrayGraph.traversalDFS(0);
    }
}
