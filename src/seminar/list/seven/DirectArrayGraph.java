package seminar.list.seven;

public class DirectArrayGraph {
    private int nodeCount;
    private int graph[][];
    private int visit[];

    DirectArrayGraph(int nodeCount) {
        this.nodeCount = nodeCount;
        this.graph = new int[nodeCount][nodeCount];
        this.visit = new int[nodeCount];
    }

    int addEdge(int fromNode, int toNode) {
        int ret = 0;

        if (graph != null && checkValid(fromNode) && checkValid(toNode)) {
            graph[fromNode][toNode] = 1;
        } else {
            ret = -1;
        }

        return ret;
    }

    int removeEdge(int fromNode, int toNode) {
        int ret = 0;

        if (graph != null && checkValid(fromNode) && checkValid(toNode)) {
            graph[fromNode][toNode] = 0;
        } else {
            ret = -1;
        }

        return ret;
    }

    int getEdge(int fromNode, int toNode) {
        int ret = 0;

        if (graph != null && checkValid(fromNode) && checkValid(toNode)) {
            ret = graph[fromNode][toNode];
        }

        return ret;
    }

    void display() {
        int i = 0, j = 0;
        int count = 0;
        if (graph != null) {
            count = nodeCount;
            for (i=0; i<count; i++) {
                for (j=0; j < count; j++) {
                    System.out.print(getEdge(i, j));
                }
                System.out.println();
            }
        }
    }

    void delete() {
        int i = 0;

        if (graph != null) {
            for (i=0; i<nodeCount; i++) {
                graph[i] = null;
            }
            graph = null;
        }
    }

    boolean checkValid(int node) {
        if (graph != null && node < nodeCount && node >= 0) {
            return true;
        }
        else return false;
    }

    void traversalDFS(int startNode) {
        visit[startNode] = 1;

        System.out.println("노드 방문 : " + startNode);
        for (int i=0; i < nodeCount; i++) {
            if (i != startNode) {
                if (getEdge(startNode, i) != 0) {
                    if (visit[i] == 0) {
                        traversalDFS(i);
                    }
                }
            }
        }
    }
}
