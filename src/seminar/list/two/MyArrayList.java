package seminar.list.two;


class MyArrayList extends MyList {

    private int[] array;
    private int maxCount;
    private int currentCount;

    @Override
    public void create(int count) {
        array = new int[count];
        maxCount = count;
        currentCount = 0;
    }

    @Override
    public void addData(int position, int data) {
        if (position > maxCount) return ;
        if (position >= currentCount) {
            array[currentCount] = data;
            currentCount++;
        }
        else {
            for (int i=currentCount; i>position; i--) {
                array[i] = array[i-1];
            }
            array[position] = data;
            currentCount++;
        }
    }

    @Override
    public int getLength() {
        return currentCount;
    }

    @Override
    public int getData(int position) {
        return array[position];
    }

    @Override
    public void removeData(int position) { // 예외 처리
        if (position > maxCount) return;
        if (position <= currentCount) {
            for(int i=position; i<currentCount; i++) {
                array[i] = array[i+1];
            }
        } else {
            array[currentCount] = 0;
        }


        currentCount--;
    }

    @Override
    public void clear() {
        for(int i=0; i<currentCount; i++) {
            array[i] = 0;
        }
        currentCount = 0;
    }

    @Override
    public void delete() {
        array = null;
    }
}