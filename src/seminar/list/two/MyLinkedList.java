package seminar.list.two;


class MyLinkedList extends MyList {

    private Node header;
    private int count;

    @Override
    public void create(int count) {
        header = new Node(0);
        count = 0;
    }

    @Override
    public void addData(int position, int data) {
        Node newNode = new Node(data);

        if (header.getNextNode() == null) {
            header.setNextNode(newNode);
            count++;
            return;
        }

        Node tmp = header;
        if (position < 0) {
            System.out.println("이 위치에 넣을 수 없습니다.");
        } else if (position < count){
            for (int i=0; i<position; i++) {
                tmp = tmp.getNextNode();
            }
            newNode.setNextNode(tmp.getNextNode());
            tmp.setNextNode(newNode);
            count++;
        } else {
            for (; tmp.getNextNode() != null; tmp = tmp.getNextNode());
            tmp.setNextNode(newNode);
        }
    }

    @Override
    public int getData(int position) {
        Node tmp;
        int i=0;

        for (tmp = header; i != position; tmp = tmp.getNextNode()) i++;

        return tmp.getData();
    }

    @Override
    public int getLength() {
        return count;
    }

    @Override
    public void removeData(int position) {
        Node tmp = header;

        if (position < 0) {
            System.out.println("제거할 수 없습니다.");
        } else if (position < count){
            for (int i=0; i<position-1; i++) {
                tmp = tmp.getNextNode();
            }
            tmp.getNextNode().setNextNode(tmp.getNextNode().getNextNode());
            count--;
        } else {
            for (int i=0; i<count-1; i++) {
                tmp = tmp.getNextNode();
            }
            tmp.setNextNode(null);
            count--;
        }
    }

    @Override
    public void clear() {
        header.setNextNode(null);
        count = 0;
    }

    @Override
    public void delete() {
        header = null;
        count = 0;
    }
}