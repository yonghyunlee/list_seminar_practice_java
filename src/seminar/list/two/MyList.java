package seminar.list.two;

abstract class MyList {
    public abstract void create(int count);
    public abstract void addData(int position, int data);
    public abstract int getData(int position);
    public abstract int getLength();
    public abstract void removeData(int position);
    public abstract void clear();
    public abstract void delete();
}