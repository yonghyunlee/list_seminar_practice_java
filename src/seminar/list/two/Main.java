package seminar.list.two;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Main {
    public static void main(String[] args) {
        MyArrayList myArrayList = new MyArrayList();
        myArrayList.create(10);

        myArrayList.addData(0, 10);
        myArrayList.addData(1, 20);
        myArrayList.addData(2, 30);
        myArrayList.addData(2, 40);
        myArrayList.removeData(0);
        for (int i=0; i<myArrayList.getLength(); i++) {
            System.out.println(myArrayList.getData(i));
        }

        myArrayList.clear();
        System.out.println(myArrayList.getLength());

//        myArrayList.delete();

//        MyLinkedList myLinkedList = new MyLinkedList();
//        myLinkedList.create(0);
//
//        myLinkedList.addData(0, 10);
//        myLinkedList.addData(0, 20);
//        System.out.println(myLinkedList.getData(0));
//        System.out.println(myLinkedList.getData(1));
//        System.out.println(myLinkedList.getData(2));
//        System.out.println(myLinkedList.getLength());
//        myLinkedList.removeData(2);
//        System.out.println(myLinkedList.getLength());
//        myLinkedList.clear();
//        System.out.println(myLinkedList.getLength());
////        myLinkedList.delete();
//        System.out.println(myLinkedList.getData(0));
    }
}