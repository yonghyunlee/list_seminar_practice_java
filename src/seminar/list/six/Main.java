package seminar.list.six;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Heap heap = new Heap(10);

        while (true) {
            int n = scanner.nextInt();
            if (n == -1) break;
            heap.push(n);
        }

        while (heap.getCurrent() > 0)  {
            System.out.println(heap.removeMaxHeapAH());
        }
    }
}
