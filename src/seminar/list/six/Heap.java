package seminar.list.six;

public class Heap {
    private int array[];
    private int current;
    private int maxCount;

    Heap(int maxCount) {
        array = new int[maxCount];
        current = 0;
        this.maxCount = maxCount;
    }


    public int push(int n) {
        int i=0;

        if (array != null) {
            if (current == maxCount) {
                System.out.println("히프가 가득 참.");
                return 0;
            }
            current++;
            i = current;
            while ((i != 1) && (n > array[i/2])){
                array[i] = array[i/2];
                i /= 2;
            }
            array[i] = n;
        }
        return i;
    }

    public int removeMaxHeapAH() {
        int parent =1, child =2;
        int n = 0;

        if (array != null && current > 0) {
            n = array[1];

            int tmp = array[current];
            current--;

            while(child <= current) {
                if ((child < current) && array[child] < array[child+1]) {
                    child++;
                }
                if (tmp >= array[child]) {
                    break;
                }
                array[parent] = array[child];
                parent = child;
                child *= 2;
            }
            array[parent] = tmp;
        }
        return n;
    }

    int getCurrent () {
        return current;
    }
}
