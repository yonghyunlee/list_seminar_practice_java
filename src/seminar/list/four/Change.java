package seminar.list.four;

import java.util.ArrayList;
import java.util.Stack;

public class Change {

    private String[] tokens;
    private Stack<String> tempStack;
    private ArrayList<String> result;

    Change(String expression) {
        tokens = expression.split(" ");
        tempStack = new Stack<>();
        result = new ArrayList<>();
    }

    public void changeExpression() {

        for (String token : tokens) {
            if (isInteger(token)) result.add(token);
            else {
                if (tempStack.empty()) {
                    tempStack.push(token);
                } else if (token.equals("(")) {
                    tempStack.push(token);
                }
                else if (token.equals(")")) {
                    String op = tempStack.pop();
                    while (!op.equals("(")) {
                        result.add(op);
                        op = tempStack.pop();
                    }
                } else if (operationPriority(token) > operationPriority(tempStack.peek())) {
                    tempStack.push(token);
                } else {
                    result.add(tempStack.pop());
                    result.add(token);
                }
            }
        }

        while (!tempStack.isEmpty()) {
            result.add(tempStack.pop());
        }
    }

    public static boolean isInteger(String token) {
        try {
            Integer.parseInt(token);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    public static int operationPriority(String op) {
        switch (op) {
            case "*" :
            case "/" :
                return 2;
            case "+" :
            case "-" :
                return 1;
            case "(" :
                return 0;
            default:
                return -1;
        }
    }

    public ArrayList<String> getResult() {
        return result;
    }
}
