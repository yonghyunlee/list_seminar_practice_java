package seminar.list.four;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        String expression;
        Scanner scan = new Scanner(System.in);

        System.out.println("수식 입력");
        expression = scan.nextLine();

        Change change = new Change(expression);
        change.changeExpression();

        System.out.println(change.getResult());
        Calculate calculate = new Calculate(change.getResult());
        System.out.println(calculate.getResult());
    }
}
