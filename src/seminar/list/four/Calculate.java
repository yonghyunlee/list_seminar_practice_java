package seminar.list.four;

import java.util.ArrayList;
import java.util.Stack;

public class Calculate {
    private ArrayList<String> expression;
    private Stack<String> stack;

    Calculate(ArrayList<String> expression) {
        this.expression = expression;
        stack = new Stack<>();

        for (String token : expression) {
            if (Change.isInteger(token)) stack.push(token);
            else {
                String op1 = stack.pop();
                String op2 = stack.pop();
                stack.push(String.valueOf(op2Calculate(op1, op2, token)));
            }
        }
    }

    public static int op2Calculate(String op1, String op2, String op) {
        int v1 = Integer.parseInt(op1);
        int v2 = Integer.parseInt(op2);
        switch (op) {
            case "*" :
                return v1 * v2;
            case "/" :
                return v2 / v1;
            case "+" :
                return v1 + v2;
            case "-" :
                return v2 - v1;
                default:
                    return 0;
        }
    }

    public String getResult() {
        return stack.pop();
    }
}
