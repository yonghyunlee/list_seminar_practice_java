package seminar.list.three;

import java.time.LocalDate;
import java.util.Date;

class Node {

    private Date start;
    private Date end;
    private String todo;
    private Node nextNode;
    public Boolean complete;
    public Boolean fail;

    Node(String todo) {
        this.todo = todo;
        this.nextNode = null;
        this.complete = false;
        this.fail = false;
    }

    public Date getStart() {
        return start;
    }

    public void setStart(Date start) {
        this.start = start;
    }

    public Date getEnd() {
        return end;
    }

    public void setEnd(Date end) {
        this.end = end;
    }

    public String getTodo() {
        return todo;
    }

    public void setTodo(String todo) {
        this.todo = todo;
    }

    public Node getNode() {
        return this;
    }

    public Node getNextNode() {
        return nextNode;
    }

    public void setNextNode(Node nextNode) {
        this.nextNode = nextNode;
    }
}