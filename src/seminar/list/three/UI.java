package seminar.list.three;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.text.DateFormatter;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.*;

public class UI extends JFrame {

    private JTextField textField;
    private JFormattedTextField startDateText;
    private JFormattedTextField endDateText;
    private JButton addBt;
    private JButton completeBt;
    private JButton removeBt;
    private JButton detailBt;
    private DetailListDialog detailListDialog;
    private JLabel jLabelToDo;
    private JLabel jLabelStartDate;
    private JLabel jLabelEndDate;
    private JLabel jLabelInfor;
    private JLabel jLabelDDay;
    private JList jList;
    private JScrollPane jScrollPane;

    private SimpleDateFormat format;

    private MyLinkedList myLinkedList;
    private Vector vector;

    UI(MyLinkedList myLinkedList) {
        super("todo-list");
        setLayout(new FlowLayout(FlowLayout.LEFT, 30, 40));
        setSize(800, 1000);

        this.myLinkedList = myLinkedList;

        vector = new Vector();

        format = new SimpleDateFormat("yyyy.MM.dd");

        jLabelToDo = new JLabel();
        jLabelToDo.setText("Something to do : ");
        jLabelToDo.setFont(new Font("Serif", Font.BOLD, 30));

        addBt = new JButton("add");
        addBt.setFont(new Font("Serif", Font.PLAIN, 30));

        textField = new JTextField(17);
        textField.setFont(new Font("Serif", Font.PLAIN, 30));
        textField.setBorder(new EmptyBorder(0, 0, 0, 0));

        jLabelStartDate = new JLabel();
        jLabelStartDate.setText("Start Date : ");
        jLabelStartDate.setFont(new Font("Serif", Font.BOLD, 30));
        jLabelStartDate.setBorder(new EmptyBorder(0, 0, 0, 77));


        startDateText = new JFormattedTextField(format);
        startDateText.setValue(new Date());
        startDateText.setColumns(10);
        startDateText.setFont(new Font("Serif", Font.BOLD, 30));
        startDateText.setBorder(new EmptyBorder(0, 0, 0, 0));

        jLabelEndDate = new JLabel();
        jLabelEndDate.setText("End Date : ");
        jLabelEndDate.setFont(new Font("Serif", Font.BOLD, 30));
        jLabelEndDate.setBorder(new EmptyBorder(0, 0, 0, 88));

        endDateText = new JFormattedTextField(format);
        endDateText.setValue(new Date());
        endDateText.setColumns(10);
        endDateText.setFont(new Font("Serif", Font.BOLD, 30));
        endDateText.setBorder(new EmptyBorder(0, 0, 0, 0));

        jList = new JList();
        jList.setListData(vector);
        jList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        JListCellRenderer jListCellRenderer = new JListCellRenderer();
        jListCellRenderer.setFont(new Font("Serif", Font.PLAIN, 30));
        Dimension dimension = jList.getPreferredSize();
        dimension.width = 500;
        jList.setCellRenderer(jListCellRenderer);

        completeBt = new JButton("complete");
        completeBt.setEnabled(false);
        completeBt.setFont(new Font("Serif", Font.BOLD, 40));

        removeBt = new JButton("remove");
        removeBt.setEnabled(false);
        removeBt.setFont(new Font("Serif", Font.BOLD, 40));

        detailBt = new JButton("detail");
        detailBt.setFont(new Font("Serif", Font.BOLD, 40));

        jScrollPane = new JScrollPane(jList);
        jScrollPane.setPreferredSize(new Dimension(700, 300));

        jLabelInfor = new JLabel();
        jLabelInfor.setFont(new Font("Serif", Font.BOLD, 30));

        jLabelDDay = new JLabel();
        jLabelDDay.setFont(new Font("Serif", Font.BOLD, 30));

        add(jLabelToDo);
        add(textField);
        add(jLabelStartDate);
        add(startDateText);
        add(jLabelEndDate);
        add(endDateText);
        add(addBt);
        add(jScrollPane);
        add(completeBt);
        add(removeBt);
        add(detailBt);
        add(Box.createRigidArea(new Dimension(500, 0)));
        add(jLabelInfor);
        add(jLabelDDay);
        setVisible(true);

        addBt.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                try {
                    Date startDate = format.parse(startDateText.getText());
                    Date endDate = format.parse(endDateText.getText());
                    myLinkedList.addNode(100, textField.getText(), startDate, endDate);
                } catch (ParseException e1) {
                    e1.printStackTrace();
                }

                vector.addElement(myLinkedList.getNode(myLinkedList.getLength()).getTodo());
                jList.setListData(vector);

                textField.setText("");
                jLabelInfor.setText("추가 완료");
            }
        });

        completeBt.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                myLinkedList.getNode(jList.getSelectedIndex()+1).complete = true;

                jList.repaint();

            }
        });

        removeBt.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println(jList.getSelectedIndex());
                myLinkedList.removeData(jList.getSelectedIndex()+1);
                vector.removeElementAt(jList.getSelectedIndex());

                jList.repaint();
            }
        });

        detailBt.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                detailListDialog = new DetailListDialog(myLinkedList);
            }
        });
    }


    private class JListCellRenderer extends DefaultListCellRenderer {

        @Override
        public Component getListCellRendererComponent(JList<?> list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
            String text = value.toString();
            setText(text);

            setForeground(Color.BLACK);

            if(myLinkedList.getNode(index+1).fail) {
                setForeground(Color.RED);
            }

            if(myLinkedList.getNode(index+1).complete) {
                setForeground(Color.BLUE);
            }


            if (isSelected) {
                setBackground(Color.GRAY);
                completeBt.setEnabled(true);
                removeBt.setEnabled(true);

                Node node = myLinkedList.getNode(index+1);
                Date startDate = node.getStart();
                Date endDate = node.getEnd();
                Date now = new Date();

                System.out.println(endDate.getTime()+(1000*60*60*24));
                if (now.getTime() > endDate.getTime()+(1000*60*60*24)) {
                    jLabelInfor.setText(format.format(startDate) + "에서 " + format.format(endDate) + "까지 해야합니다.");
                    jLabelDDay.setText("실패했습니다.");
                    myLinkedList.getNode(jList.getSelectedIndex()+1).fail = true;

                    return this;
                }

                jLabelInfor.setText(format.format(startDate) + "에서 " + format.format(endDate) + "까지 해야합니다.");
                if (now.getTime() > endDate.getTime()) {
                    jLabelDDay.setText("오늘까지 해야합니다.");
                } else {
                    jLabelDDay.setText((endDate.getTime()+(1000*60*60*24)-now.getTime()) / (1000*60*60*24) + "일 남았습니다.");
                }
            } else {
                setBackground(Color.WHITE);
            }

            return this;
        }
    }
}

class DetailListDialog extends JFrame {
    private JLabel todayLabel;
    private JLabel completeLabel;
    private JLabel failLabel;
    private JList todayList;
    private JList completeList;
    private JList failList;
    private JScrollPane todayJScrollPane;
    private JScrollPane completeJScrollPane;
    private JScrollPane failJScrollPane;
    private Vector todayVector;
    private Vector completeVector;
    private Vector failVector;
    private SimpleDateFormat format;

    public DetailListDialog(MyLinkedList myLinkedList) {
        super("detail-list");
        setLayout(new FlowLayout(FlowLayout.LEFT, 20, 30));
        setSize(800, 900);

        todayVector = new Vector();
        completeVector = new Vector();
        failVector = new Vector();

        format = new SimpleDateFormat("yyyy.MM.dd");

        todayLabel = new JLabel("today List");
        todayLabel.setFont(new Font("Serif", Font.BOLD, 30));

        completeLabel = new JLabel("complete list");
        completeLabel.setFont(new Font("Serif", Font.BOLD, 30));

        failLabel = new JLabel("fail list");
        failLabel.setFont(new Font("Serif", Font.BOLD, 30));


        todayList = new JList();
        Date now = new Date();
        for(int i=1; i<myLinkedList.getLength()+1; i++) {
            if (myLinkedList.getNode(i).complete || myLinkedList.getNode(i).fail) continue;
            Date start = myLinkedList.getNode(i).getStart();
            Date end = myLinkedList.getNode(i).getEnd();
            if (start.getTime() < now.getTime() && end.getTime() > now.getTime()) {
                todayVector.addElement(myLinkedList.getNode(i).getTodo()  + " " + format.format(end));
            }
        }
        todayList.setListData(todayVector);
        todayList.setFont(new Font("Serif", Font.PLAIN, 30));
        todayJScrollPane = new JScrollPane(todayList);
        todayJScrollPane.setPreferredSize(new Dimension(730, 300));


        completeList = new JList();
        for(int i=1; i<myLinkedList.getLength()+1; i++) {
            System.out.println(myLinkedList.getNode(i).getTodo() + myLinkedList.getNode(i).complete);
            if (myLinkedList.getNode(i).complete) {
                Node node = myLinkedList.getNode(i);
                completeVector.addElement(node.getTodo() + " " + format.format(node.getEnd()));
            }

        }
        completeList.setListData(completeVector);
        completeList.setFont(new Font("Serif", Font.PLAIN, 30));
        completeJScrollPane = new JScrollPane(completeList);
        completeJScrollPane.setPreferredSize(new Dimension(300, 300));

        failList = new JList();
        for(int i=1; i<myLinkedList.getLength()+1; i++) {
            if (myLinkedList.getNode(i).fail) {
                Node node = myLinkedList.getNode(i);
                failVector.addElement(node.getTodo() + " " + format.format(node.getEnd()));
            }
        }
        failList.setListData(failVector);
        failList.setFont(new Font("Serif", Font.PLAIN, 30));
        failJScrollPane = new JScrollPane(failList);
        failJScrollPane.setPreferredSize(new Dimension(300, 300));

        add(todayLabel);
        add(todayJScrollPane);
        add(completeLabel);
        add(Box.createRigidArea(new Dimension(450, 0)));
        add(failLabel);
        add(completeJScrollPane);
        add(Box.createRigidArea(new Dimension(100, 0)));
        add(failJScrollPane);

        setVisible(true);
    }
}