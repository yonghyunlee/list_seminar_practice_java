package seminar.list.three;

import java.util.Date;

class MyLinkedList extends MyList {

    private Node header;
    private int count;

    @Override
    public void create() {
        header = new Node(null);
        count = 0;
    }

    @Override
    public void addNode(int position, String todo, Date start, Date end) {
        Node newNode = new Node(todo);
        newNode.setStart(start);
        newNode.setEnd(end);

        if (header.getNextNode() == null) {
            header.setNextNode(newNode);
            count++;
            return;
        }

        Node tmp = header;
        if (position < 0) {
            System.out.println("이 위치에 넣을 수 없습니다.");
        }
        else if (position < count){
            System.out.println("\n자료의 개수보다 넣을 위치가 작음");
            for (int i=0; i<position; i++) {
                tmp = tmp.getNextNode();
            }
            newNode.setNextNode(tmp.getNextNode());
            tmp.setNextNode(newNode);
            count++;
        }
        else {
            System.out.println("\n자료의 개수보다 넣을 위치가 큼");
            for (tmp = header; tmp.getNextNode() != null; tmp = tmp.getNextNode());
            tmp.setNextNode(newNode);
            count++;
        }
    }

    @Override
    public Node getNode(int position) {
        Node tmp;
        int i=0;

        for (tmp = header; i != position; tmp = tmp.getNextNode()) i++;

        return tmp;
    }

    @Override
    public int getLength() {
        return count;
    }

    @Override
    public void removeData(int position) {
        Node tmp = header;

        if (position < 0) {
            System.out.println("제거할 수 없습니다.");
        } else if (position < count){
            System.out.println("\n자료의 개수보다 지울 위치가 작음");
            for (int i=0; i<position; i++) {
                tmp = tmp.getNextNode();
            }
            tmp.setNextNode(tmp.getNextNode().getNextNode());
            count--;
        } else {
            System.out.println("\n자료의 개수보다 지울 위치가 큼");
            for (int i=0; i<count-1; i++) {
                tmp = tmp.getNextNode();
            }
            tmp.setNextNode(null);
            count--;
        }
    }

    @Override
    public void clear() {
        header.setNextNode(null);
        count = 0;
    }

    @Override
    public void delete() {
        header = null;
        count = 0;
    }
}