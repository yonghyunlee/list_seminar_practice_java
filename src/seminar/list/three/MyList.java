package seminar.list.three;

import java.util.Date;

abstract class MyList {
    public abstract void create();
    public abstract void addNode(int position, String todo, Date start, Date end);
    public abstract Node getNode(int position);
    public abstract int getLength();
    public abstract void removeData(int position);
    public abstract void clear();
    public abstract void delete();
}