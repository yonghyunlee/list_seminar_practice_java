package seminar.list.five;

public class Tree {
    private Node root;

    Tree() {
        root = null;
    }

    Node getRoot() {
        return root;
    }

    void setRoot(String data) {
        root = new Node(data);
    }

    Node insertData(String data) {
        Node node = new Node(data);
        return node;
    }

    Node insertLeftChild(Node parentNode, String data) {
        if (parentNode != null) {
            if (parentNode.getLeftChild() == null) {
                parentNode.setLeftChild(insertData(data));
                return parentNode.getLeftChild();
            } else {
                return parentNode;
            }
        } else return parentNode;
    }

    Node insertRightChild(Node parentNode, String data) {
        if (parentNode != null) {
            if (parentNode.getRightChild() == null) {
                parentNode.setRightChild(insertData(data));
                return parentNode.getRightChild();
            } else {
                return parentNode;
            }
        } else return parentNode;
    }

    void deleteNode(Node parentNode){
        if (parentNode.getData() != null) {
            deleteNode(parentNode.getLeftChild());
            deleteNode(parentNode.getRightChild());
            parentNode.setData(null);
        }
    }

    void preOrder(Node root) {
        if (root == null) return;

        System.out.print(root.getData() + " ");
        preOrder(root.getLeftChild());
        preOrder(root.getRightChild());
    }

    void inOrder(Node root) {
        if (root == null) return;

        inOrder(root.getLeftChild());
        System.out.print(root.getData() + " ");
        inOrder(root.getRightChild());
    }

    void postOrder(Node root) {
        if (root == null) return;

        postOrder(root.getLeftChild());
        postOrder(root.getRightChild());
        System.out.print(root.getData() + " ");
    }
}
