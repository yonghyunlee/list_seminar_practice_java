package seminar.list.five;

public class Main {
    public static void main(String[] args) {
        Tree tree = new Tree();
        tree.setRoot("+");
        Node one = tree.insertLeftChild(tree.getRoot(), "*");
//        System.out.println(one.getData());
        Node two = tree.insertRightChild(tree.getRoot(), "/");
//        System.out.println(two.getData());
        tree.insertLeftChild(one, "2");
        Node three = tree.insertRightChild(one, "-");
//        System.out.println(three.getData());
        tree.insertRightChild(three, "3");
        tree.insertLeftChild(three, "5");
        Node four = tree.insertLeftChild(two, "%");
//        System.out.println(four.getData());
        tree.insertRightChild(two, "9");
        tree.insertLeftChild(four, "10");
        tree.insertRightChild(four, "7");

        tree.preOrder(tree.getRoot());
        System.out.println();

        tree.inOrder(tree.getRoot());
        System.out.println();
        tree.postOrder(tree.getRoot());
        System.out.println();
    }
}