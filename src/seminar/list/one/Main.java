package seminar.list.one;

public class Main {


    public static void main(String[] args) {
        System.out.println(four(10));
        System.out.println(five(10));
        System.out.println(six(10));
        System.out.println(factorial_iterator(10));
        System.out.println(fib(4));
    }

    private static int four(int n) {
        if(n == 1) {
            return 1;
        }
        return n+four(n-1);
    }


    private static float five(float n) {
        if(n == 1) {
            return 1;
        }
        return 1/n+five(n-1);
    }


    private static int six(int n) {
        int result = 0;
        for(int i=0; i<n; i++) {
           result += i+1;
        }
        return result;
    }


    private static int factorial_iterator(int n) {
        int ret = 1;
        for(int i = n; i > 1; i--) {
            ret = ret * i;
        }
        return ret;
    }


    private static int fib(int n) {
        int ret;

        if (n == 0) {
            ret = 0;
        } else if(n==1) {
            ret = 1;
        } else {
            ret = fib(n-1)+fib(n-2);
        }

        return ret;
    }
}
