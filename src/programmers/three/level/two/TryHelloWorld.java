package programmers.three.level.two;

public class TryHelloWorld {
    public int nextBigNumber(int n)
    {
        int answer = n;
        int count = 0;
        String binary = Integer.toBinaryString(n);
        for (int i=0; i<binary.length(); i++) {
            if (binary.charAt(i)=='1') {
                count++;
            }
        }

        while (true) {
            answer++;
            String tmp = Integer.toBinaryString(answer);
            int tmpCount = 0;
            for (int i=0; i<tmp.length(); i++) {
                if (tmp.charAt(i)=='1') {
                    tmpCount++;
                }
            }
            if (tmpCount == count) {
                break;
            }
        }

        return answer;
    }
    public static void main(String[] args)
    {
        TryHelloWorld test = new TryHelloWorld();
        int n = 78;
        System.out.println(test.nextBigNumber(n));
    }
}
